### Mobiquity Test

## Description

I made this App with a personal boilerplate that I made 1 year ago, that's why the version of React, ESLint and other libraries is old. I use my own boilerplate because I don't want to lose time with the configuration. The current project has Babel, React, ESLint, CSS, Image, Fonts, HTML Extract Plugin and define plugin configured with Webpack 2.

I used PubSub to connect child components with parent components. I found this observer pattern much easy and simple than Redux, I think Redux should be use only when is required. For example when you required to know how the state has been changing or in case you need to restore your app base on a previous state.

Also I added Yahoo's library for internationalization "React-Intl". I have been using it for long time without any problem.

I use CSS because my boilerplate was using only css and also because is easy for me. I'm used to work with LESS but for this case I want to keep it as easy as possible. Another option is CSS-IN-JS or Styled-Components but I personally prefer to have a stylesheet that I could change without have to care about what styles has the component by default. 

This App show you the F1 Champions from 2005 to 2015 using an API from http://ergast.com/. You can select a year to see more information about that Season. If you want more detail, you can see information about the race and which drivers finished the race.

## Install
To install type this in your console:

`yarn install`

## Production
To build a production bundle type this in your console:

`yarn dev`

## Dev
To start the boilerplate in dev mode type this in your console:

`yarn dev`

## Test
To run the unit testing for Components type this in your console:

`yarn test`

## TODO's

1. At the beginning I was thinking to implement a local cache system but I get out of time

2.  The Error System Notification is too light, does not describe very well the errors.

3. The theme is not full responsive, the time I have to spend on this test is not enough to make it full responsive.