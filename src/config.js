import usaIcon from 'assets/img/usa_64.png'
import colIcon from 'assets/img/colombia_64.png'

const config = {
    defaultLang: 'es',
    languages: [{ code: 'es', icon: colIcon, name:'Español' }, { code: 'en', icon: usaIcon, name: 'English' }],
    QUERY_LIMIT: {
        FROM: 2005,
        TO: 2015
    }
}

let host = 'http://localhost:3000'

if (process.env.NODE_ENV === 'production') {
    host = 'http://www.mobiquityinc.com'
    Object.assign(config, {
        host,
        apiHost: 'http://ergast.com/api',
    })
} else if (process.env.NODE_ENV === 'qa') {
    Object.assign(config, {
        host,
        apiHost: 'http://ergast.com/api',
    })
} else {
    Object.assign(config, {
        host,
        apiHost: 'http://ergast.com/api',
    })
}

export default config