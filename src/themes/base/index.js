import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Grid } from 'react-bootstrap'
import { FormattedMessage } from 'react-intl'
import Config from 'config'
import Language from 'components/language'
import Helper from 'libs/helper'

class BaseTheme extends Component {
    constructor (props) {
        super(props)

        this.onChangeLanguage = this.onChangeLanguage.bind(this)
    }

    onChangeLanguage (lang) {
        Helper.PubSub.publish(Helper.EVENTS.SET_APP_LANG, lang)
    }

    render () {
        return (
            <Grid className='base-theme'>
                <header>
                    <h1><FormattedMessage id='app.home.title' /></h1>
                    <Language
                      onClick={this.onChangeLanguage}
                      langs={Config.languages}
                      current={this.props.route.appInfo.lang.locale}
                    />
                </header>
                {
                    !this.props.children ? this.props.router.push('/') : this.props.children
                }
                <footer>
                    <Row>
                        <Col lg={12} md={12} sm={12} xs={12}>
                            <FormattedMessage id='app.copyright' />
                        </Col>
                    </Row>
                </footer>
            </Grid>
        )
    }
}

BaseTheme.propTypes = {
    route: PropTypes.any.isRequired,
    children: PropTypes.any.isRequired,
    router: PropTypes.any.isRequired
}

export default BaseTheme