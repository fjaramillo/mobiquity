import Api from 'libs/api'
import Config from 'config'
import Helper from 'libs/helper'
import { FormattedMessage } from 'react-intl'

const _details = {}
let _data = []
const _errors = []

// TODO: Make cache system base on the api queries

/*
    DESCRIPTION: Function to get the initial data base on the limit set up in config
    PARAMS:
    from: Minimum year valid
    to: Maximum year valid
    force: Flag to call the API or returned an old result (Simple Cache)
*/
async function _getYearsDetail (from = 2005, to = 2015, force = false) {
    if (_data.length === 0 || force) {
        _data = []
        const res = await Api.getJSONP(Api.ENDPOINTS.QUALIFICATION_BY_YEAR)

        if (Helper.responseValidate(res)) {
            res.MRData.StandingsTable.StandingsLists.map((info) => {
                const year = parseInt(info.season)
                // Condition to show only the years configured
                if (year < Config.QUERY_LIMIT.FROM || year > Config.QUERY_LIMIT.TO) {
                    return;
                }

                const valid = !!(info && info.DriverStandings)
                const driver = valid ? info.DriverStandings[0].Driver : false

                const item = {
                    year,
                    winner: buildName(driver)
                }
                _data.push(item)
            })

        } else {
            _errors.push({ msg: <FormattedMessage id='app.ajaxError' /> })
        }

    }

    return {
        errors: _errors,
        array: _data
    }
}

/*
    DESCRIPTION: Function to get the season data base on a year
    PARAMS:
    year: Year that we want to search
    force: Flag to call the API or returned an old result (Simple Cache)
*/
async function _getSeasonDetail (year, force = false) {
    // Cache should work here base on Year
    if (!_details[year] || _details[year].length === 0 || force) {
        const _temp = []
        const res = await Api.getJSONP(Api.ENDPOINTS.WINNERS_BY_SEASON, { year }, true)

        if (Helper.responseValidate(res)) {
            res.MRData.RaceTable.Races.map((info) => {
                const year = parseInt(info.season)
                // Condition to show only the years configured
                if (year < Config.QUERY_LIMIT.FROM || year > Config.QUERY_LIMIT.TO) {
                    return;
                }

                const item = {
                    race: info.Circuit.circuitName,
                    winner: buildName(info.Results[0].Driver),
                    year,
                    round: info.round,
                    time: info.Results[0].Time.time
                }
                _temp.push(item)
            })
            _details[year] = _temp
        } else {
            _errors.push({ msg: <FormattedMessage id='app.ajaxError' /> })
        }
    }

    return {
        errors: _errors,
        array: _details[year]
    }
}

function _getRaceDetail () {
    return []
}

function buildName (driver) {
    return driver ? `${driver.givenName} ${driver.familyName} (${driver.nationality})` :
    <FormattedMessage id='app.notfound' />
}

export default {
    getYearChampions: _getYearsDetail,
    getSeason: _getSeasonDetail,
    getRace: _getRaceDetail
}