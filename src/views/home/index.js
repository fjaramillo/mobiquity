import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl'
import { Row, Col, Button } from 'react-bootstrap'
import classnames from 'classnames'
import Helper from 'libs/helper'
import Card from 'components/card'
import Loader from 'components/loader'

import Model from './model'

class Home extends Component {
    constructor (props) {
        super(props)

        this.state = {
            selectItem: null,
            selectWinner: null,
            data: [],
            details: [],
            loaded: false,
            detailsLoaded: false
        }
        // Flag to know if we use the animation with delay
        this.detailsFirstTime = true

        this.onSelectRow = this.onSelectRow.bind(this)
        this.renderList = this.renderList.bind(this)
        this.renderDetail = this.renderDetail.bind(this)
    }

    async componentDidMount () {
        const res = await Model.getYearChampions()

        if (res.errors.length === 0) {
            // To Implement Local Cache System
            // Helper.setLocalData('data', res.array)
            this.setState({
                data: res.array,
                loaded: true
            })
        } else {
            Helper.addNotification(<FormattedMessage id='app.home.dataError' />, null, 'warning')
        }
    }

    onSelectRow (item) {
        this.setState({
            detailsLoaded: false
        }, async () => {
            const res = await Model.getSeason(item.year)

            if (res.errors.length === 0) {
                // To Implement Local Cache System
                // Helper.setLocalData(`detailsData-${item.year}`, res.array)
                this.setState({
                    details: res.array,
                    detailsLoaded: true,
                    selectItem: item.year,
                    selectWinner: item.winner
                }, () => {
                    this.detailsFirstTime = false
                })
            } else {
                Helper.addNotification(<FormattedMessage id='app.home.dataError' />, null, 'warning')
            }
        })
    }

    openDetails (year, round) {
        this.props.router.push(`detail/?year=${year}&round=${round}`)
    }

    renderDetail (item) {
        const { selectWinner } = this.state
        return (
            <tr key={`${item.race}`} className={classnames({ 'highlight': item.winner === selectWinner })}>
                <td><i className='glyphicon glyphicon-road' /> {item.race}</td>
                <td><i className='glyphicon glyphicon-user' /> {item.winner}</td>
                <td><i className='glyphicon glyphicon-time' /> {item.time}</td>
                <td>
                    <Button  onClick={() => this.openDetails(item.year, item.round)}>
                        <FormattedMessage id='app.details' />
                    </Button>
                </td>
            </tr>
        )
    }

    renderList (item) {
        const { selectItem } = this.state
        return (
            <tr
              key={`${item.year}`}
              onClick={() => this.onSelectRow(item)}
              className={classnames({ 'highlight': item.year === selectItem }, 'link')}
            >
                <td><i className='glyphicon glyphicon-calendar' /> {item.year}</td>
                <td><i className='glyphicon glyphicon-tower' /> {item.winner}</td>
            </tr>
        )
    }

    render () {
        const _seasonTitle = (<h4 className='title'>
            <FormattedMessage id='app.home.season.title' values={{ year: this.state.selectItem }} />
        </h4>)
        return (
            <Row className='view-home body'>
                <Col lg={12} md={12} sm={12} xs={12}>
                    <Loader loaded={this.state.loaded}>
                        <Card
                          title={<h4 className='title'><FormattedMessage id='app.home.card.title' /></h4>}
                          subtitle={<FormattedMessage id='app.home.card.subtitle' />}
                          items={this.state.data}
                          render={this.renderList}
                          fixed
                          className={classnames({
                              compact: this.state.selectItem > 0,
                              full: this.state.selectItem === null
                          })}
                        />
                    </Loader>
                    {
                        this.state.selectItem > 0 && <Loader loaded={this.state.detailsLoaded}>
                            <Card
                              title={_seasonTitle}
                              subtitle={<FormattedMessage id='app.home.season.subtitle' />}
                              items={this.state.details}
                              render={this.renderDetail}
                              fixed
                              className={classnames({
                                  showcard: this.detailsFirstTime,
                                  fadeIn: !this.detailsFirstTime,
                                  hidecard: this.state.selectItem === null
                              })}
                            />
                        </Loader>
                    }
                </Col>
            </Row>
        );
    }
}

Home.propTypes = {
    router: PropTypes.any.isRequired
}

export default Home;