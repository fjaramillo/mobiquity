import React, { Component, PropTypes } from 'react'
import { Row, Jumbotron, Button } from 'react-bootstrap'
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl'
import Loader from 'components/loader'
import Card from 'components/card'
import Helper from 'libs/helper'

import Model from './model'

class Details extends Component {
    constructor (props) {
        super(props)

        this.state = {
            loaded: false,
            data: [],
            year: 0,
            round: 0
        }

        this.renderList = this.renderList.bind(this)
        this.goBack = this.goBack.bind(this)
    }

    async componentDidMount () {
        const { year, round } = this.props.location.query
        const res = await Model.getRace(year, round)

        if (res.errors.length === 0) {
            // To Implement Local Cache System
            // Helper.setLocalData(`details-race-${year}-${round}`, res.array)
            this.setState({
                data: res.array,
                loaded: true,
                year,
                round
            })
        } else {
            Helper.addNotification(<FormattedMessage id='app.home.dataError' />, null, 'warning')
        }
    }

    goBack () {
        this.props.router.push('/home')
    }

    renderList (item) {
        return (
            <tr key={`${item.year}-${item.position}`}>
                <td><i className='glyphicon glyphicon-calendar' /> {item.position}</td>
                <td><i className='glyphicon glyphicon-user' /> {item.name}</td>
                <td><i className='glyphicon glyphicon-flag' /> {item.team}</td>
                <td><i className='glyphicon glyphicon-time' /> {item.time}</td>
                <td><i className='glyphicon glyphicon-info-sign' /> {item.status}</td>
                <td><i className='glyphicon glyphicon-ok' /> {item.points}</td>
            </tr>
        )
    }

    render () {
        const { year, round } = this.state
        return (
            <Row className='view-details'>
                <Jumbotron>
                    <h1><FormattedMessage id='app.details.title' /></h1>
                    <p>
                        <FormattedHTMLMessage id='app.details.message' /><br />
                        <Button onClick={this.goBack} bsStyle='primary'>
                            <FormattedHTMLMessage id='app.return' />
                        </Button>
                    </p>
                </Jumbotron>
                <Loader loaded={this.state.loaded}>
                    <Card
                      title={<h4 className='title'><FormattedMessage id='app.details.card.title' /></h4>}
                      subtitle={<FormattedMessage id='app.details.card.subtitle' values={{ year, round }} />}
                      items={this.state.data}
                      render={this.renderList}
                      className='s'
                    />
                </Loader>
            </Row>
        )
    }
}

Details.propTypes = {
    location: PropTypes.any.isRequired,
    router: PropTypes.any.isRequired
}

export default Details