import Api from 'libs/api'
import Helper from 'libs/helper'
import { FormattedMessage } from 'react-intl'

const _data = {}
const _errors = []

// TODO: Make local cache system base on the api queries

/*
    DESCRIPTION: Function to get the initial data base on the Race
    PARAMS:
    year: season of the race
    round: Race ID
    force: Flag to call the API or returned an old result (Simple Cache)
*/
async function _getRaceDetail (year, round, force = false) {
    // Cache should work here base on Year and Race
    if (!_data[year] || !_data[year][round] || _data[year][round].length === 0 || force) {
        const _temp = []
        const res = await Api.getJSONP(Api.ENDPOINTS.QUALIFICATION_BY_RACE, { year, round }, true)

        if (Helper.responseValidate(res)) {
            res.MRData.RaceTable.Races[0].Results.map((info) => {

                const item = {
                    name: buildName(info.Driver),
                    position: info.position,
                    status: info.status,
                    points: info.points,
                    team: info.Constructor.name,
                    time: info.Time ? info.Time.time : 'N/A',
                    year,
                    round
                }
                _temp.push(item)
            })
            if (_data[year]) {
                _data[year][round] = _temp
            } else {
                _data[year] = { [round]: _temp }
            }
        } else {
            _errors.push({ msg: <FormattedMessage id='app.ajaxError' /> })
        }
    }

    return {
        errors: _errors,
        array: _data[year][round]
    }
}

function buildName (driver) {
    return driver ? `${driver.givenName} ${driver.familyName} (${driver.nationality})` :
    <FormattedMessage id='app.notfound' />
}

export default {
    getRace: _getRaceDetail
}