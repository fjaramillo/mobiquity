## Language Component

This component is used to create the language bar that allow the user to change between languages

### Properties

```
{
    /* Property to catch the click on any of the buttons */
    onClick: PropTypes.func.isRequired,
    /* Property to set up the available languages */
    langs: PropTypes.array.isRequired,
    /* Property to set up which language is the current */
    current: PropTypes.string.isRequired
}
```

### Example

#### Without Items
```
<Language 
    langs={[ { name: 'Spanish', icon: 'path/to/image', code: 'es' } ]}
    current='es'
    onClick={ () => console.log('Language bar clicked') }
/>
    