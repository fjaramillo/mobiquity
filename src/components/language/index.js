import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, OverlayTrigger, Tooltip  } from 'react-bootstrap'

class Language extends Component {
    render () {
        const langs = this.props.langs
        const content = langs.map((ln) => {
            const act = ln.code === this.props.current ? 'active' : ''
            const tooltip = <Tooltip id={ln.code}>{ln.name}</Tooltip>
            return (<OverlayTrigger key={ln.code} placement='bottom' overlay={tooltip}>
                <Button bsStyle='link' data-lang={ln.code} className={act} onClick={() => this.props.onClick(ln.code)}>
                    <img src={ln.icon} width='32' height='24' alt={ln.code} />
                </Button>
            </OverlayTrigger>)
        })

        return (
            <div className='language-component bar-lang-horizontal'>{content}</div>
        )
    }
}

Language.propTypes = {
    onClick: PropTypes.func.isRequired,
    langs: PropTypes.array.isRequired,
    current: PropTypes.string.isRequired
}

export default Language