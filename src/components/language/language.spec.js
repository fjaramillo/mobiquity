import React from 'react'
import { mountWithIntl } from 'libs/test.helper'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'
import sinon from 'sinon'
import Component from './index.js'

const expect = chai.expect
chai.use(chaiEnzyme()) // We need to tell chai to use chaiEnzyme

describe('Language Component ', () => {

    it('Render', () => {
        const com = mountWithIntl(<Component 
            langs={[
                { name: 'Spanish', icon: 'path/to/image', code: 'es' },
                { name: 'English', icon: 'path/to/image', code: 'us' }
            ]}
        />)
        expect(com).to.be.ok
    });

    it('Render With Languages', () => {
        const com = mountWithIntl(<Component
            langs={[
                { name: 'Spanish', icon: 'path/to/image', code: 'es' },
                { name: 'English', icon: 'path/to/image', code: 'us' }
            ]}
            current='es' />
        )
        expect(com.find('.btn')).to.have.lengthOf(2)
        expect(com.find('.btn .active')).to.have.lengthOf(1)
        expect(com.find('.btn .active img')).to.have.attr('src', 'path/to/image')
    });

    it('Render With OnClick Event', () => {
        const onMock = sinon.spy()
        const com = mountWithIntl(<Component
            langs={[
                { name: 'Spanish', icon: 'path/to/image', code: 'es' },
                { name: 'English', icon: 'path/to/image', code: 'us' }
            ]}
            current='es' 
            onClick={onMock}
            />
        )
        com.find('.btn .active').simulate('click')
        expect(onMock.called).to.be.true
    });

});