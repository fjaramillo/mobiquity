import React from 'react'
import { mountWithIntl } from 'libs/test.helper'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'
import Component from './index.js'

const expect = chai.expect
chai.use(chaiEnzyme()) // We need to tell chai to use chaiEnzyme

describe('Card Component ', () => {

    it('Render', () => {
        const com = mountWithIntl(<Component />)
        expect(com).to.be.ok
    });

    it('Render With Title and Subtitle', () => {
        const com = mountWithIntl(<Component
          title={<h4 className='title'>Hola</h4>}
          subtitle={<h4 className='subtitle'>Hola</h4>}
        />
        )
        expect(com.find('.title')).to.have.lengthOf(1)
        expect(com.find('.subtitle')).to.have.lengthOf(1)
    });

    it('Render With Title, Subtitle and Footer', () => {
        const com = mountWithIntl(<Component
          title={<h4 className='title'>Title</h4>}
          subtitle={<h4 className='subtitle'>Subtitle</h4>}
          footer={<h4 className='footer-content'>Footer</h4>}
        />
        )
        expect(com.find('.title')).to.have.lengthOf(1)
        expect(com.find('.subtitle')).to.have.lengthOf(1)
        expect(com.find('.footer-content')).to.have.lengthOf(1)
    });

    it('Render With Items', () => {
        const com = mountWithIntl(<Component
          items={[{ name: 'a', label:'a' }, { name: 'b', label:'b' }]}
          render={i => <tr><td>{i.name}</td></tr>}
        />
        )

        expect(com.find('table')).to.have.lengthOf(1)
        expect(com.find('table tr')).to.have.lengthOf(2)
        expect(com.find('table tr td').first()).to.have.text('a')
    });

    it('Render With Childrens', () => {
        const com = mountWithIntl(<Component>
            <p className='children'>Children Content</p>
        </Component>)

        expect(com.find('.children')).to.have.lengthOf(1)
        expect(com.find('.children')).to.have.text('Children Content')
    });

});