## Card Component

This component is used to create structures like Cards. It has Title, Subtitle and a footer that shows the user how much time ago the component was updated.

### Properties

```
{
    /* Object that allows the card use the intl component to translate the content */
    intl: intlShape.isRequired,
    /* Title to display in the card, you can pass something like this: <h4>Title</h4> */
    title: PropTypes.any,
    /* Subtitle that will be displayed below the title, receives any value */
    subtitle: PropTypes.any,
    /* Content to be displayed at the footer before the TimeAgo feature */
    footer: PropTypes.any,
    /* Boolean to hide or show the TimeAgo component */
    timeago: PropTypes.bool,
    /* The component check if this property is set, if it's set display a table with the information that should come as the shape define. If it's not define It will render the children of the component. */
    items: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.any,
        name: PropTypes.string,
        actions: PropTypes.any
    })),
    /* string that will be added to the card container */
    className: PropTypes.string,
    /* Bool to add the class fixed-card, If it's fixed, the content should has a scroll bar to display the content in a maximum height of 400px. You can find the style in app.css  */
    fixed: PropTypes.bool
}
```

### Default Properties

```
{
    timeago: true,
    className: '',
    fixed: false
}
```

### Example

#### Without Items
```
<Card 
    title={<h4 className='title'>Title</h4>}
    subtitle={<p className='category'>SubTitle</p>}
    footer={<p>Footer</p>}
    className='wrapperNotification'
    fixed
>
    This Card does not display as items, It will display this text
</Card>
```
#### With Items
```
<Card 
    title={<h4 className='title'>Title</h4>}
    subtitle={<p className='category'>SubTitle</p>}
    footer={<p>Footer</p>}
    className='wrapperNotification'
    items={[
        {
            name:'a', 
            label:'a', 
            actions: <Link to='/delete'>Delete</Link> 
        }
    ]}
    fixed
>
    This content won't be show to the user.
</Card>
```