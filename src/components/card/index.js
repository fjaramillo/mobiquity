import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table } from 'react-bootstrap'
import { injectIntl, FormattedMessage, intlShape } from 'react-intl'
import classnames from 'classnames'

class Card extends Component {
    renderItems () {
        if (this.props.items.length > 0) {
            return (<div className='table-full-width'>
                <Table hover>
                    <tbody>
                        {
                            this.props.items.map((item, x) => this.props.render(item, x))
                        }
                        {
                            this.props.items.length === 0 &&
                            <tr><td colSpan='2'><FormattedMessage id='app.notresoults' /></td></tr>
                        }
                    </tbody>
                </Table>
            </div>)
        } else {
            return this.props.children
        }
    }

    render () {
        const header = (this.props.title || this.props.subtitle) && <div className='header'>
            {this.props.title}
            {this.props.subtitle}
        </div>
        const itemsClass = this.props.fixed ? 'fixed-card' : ''
        return (
            <div className={classnames('card-component', this.props.className)}>
                { header }
                <div className={`content ${itemsClass}`}>
                    {
                        this.renderItems()
                    }
                </div>
                {
                    this.props.footer &&
                    <div className='footer'>
                        <hr />
                        <div className='stats'>
                            {this.props.footer}
                        </div>
                    </div>
                }
            </div>
        );
    }
}

/* eslint react/no-unused-prop-types: 0 */

Card.propTypes = {
    /* Object that allows the card use the intl component to translate the content */
    intl: intlShape.isRequired,
    /* Title to display in the card, you can pass something like this: <h4>Title</h4> */
    title: PropTypes.any.isRequired,
    /* Function to Render the Items */
    render: PropTypes.func.isRequired,
    /* Subtitle that will be displayed below the title, receives any value */
    subtitle: PropTypes.any,
    /* Content to be displayed at the footer before the TimeAgo feature */
    footer: PropTypes.any,
    /* The component check if this property is set, if it's set display a table with the information that should come as the shape define. If it's not define It will render the children of the component. */
    items: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.any,
        name: PropTypes.string,
        actions: PropTypes.any
    })),
    /* string that will be added to the card container */
    className: PropTypes.string,
    /* Bool to add the class fixed-card, If it's fixed, the content should has a scroll bar to display the content in a maximum height of 400px. You can find the style in app.css  */
    fixed: PropTypes.bool,
    children: PropTypes.any
};

Card.defaultProps = {
    className: '',
    fixed: false,
    subtitle: '',
    footer: '',
    items: [],
    children: <div />
}

export default injectIntl(Card)