import React from 'react'
import { mountWithIntl } from 'libs/test.helper'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'
import sinon from 'sinon'
import Component from './index.js'

const expect = chai.expect
chai.use(chaiEnzyme()) // We need to tell chai to use chaiEnzyme

describe('Loader Component ', () => {

    it('Render', () => {
        const com = mountWithIntl(<Component><p>Hola</p></Component>)
        expect(com).to.be.ok
        expect(com.find('.component-loading img')).to.have.lengthOf(1)
        expect(com.find('.component-loading span')).to.have.lengthOf(1)
    });

    it('Loader hide after flag is set up', () => {
        const data = { loaded: false }
        const com = mountWithIntl(<Component loaded={data.loaded}>
            <p className='show' /> 
        </Component>)

        expect(com.find('.component-loading img')).to.have.lengthOf(1)
        com.setProps({ loaded: true })
        expect(com.find('.component-loading img')).to.have.lengthOf(0)
        expect(com.find('.show')).to.have.lengthOf(1)
        expect(com.find('img')).to.have.lengthOf(0)
    });

});