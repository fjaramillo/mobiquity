## Loader Component

This component is used to show a loader spinner until the property loaded is true

### Properties

```
{
    /* Property to indicate if the content is ready to show */
    loaded: PropTypes.bool
}
```

### Example

#### Without Items
```
<Loader loaded={this.state.loaded}>
    {this.state.data}
</Loaded>