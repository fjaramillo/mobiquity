import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import loading from 'assets/img/loading.gif'


class Loader extends Component {
    render () {
        const _loadingText = <FormattedMessage id='app.loading' />
        return (
            !this.props.loaded ? <div className='component-loading'>
                <img src={loading} alt={_loadingText} />
                { _loadingText }
            </div> : this.props.children
        )
    }
}

Loader.propTypes = {
    loaded: PropTypes.bool.isRequired,
    children: PropTypes.any.isRequired
}

Loader.defaultProps = {
    loaded: false
}

export default Loader;