import React, { Component } from 'react'
import Notification from 'react-notification-system'
import Router from 'libs/router'
import Helper from 'libs/helper'

import 'assets/css/fonts.css'
import { addLocaleData, IntlProvider } from 'react-intl';
import esLocale from 'locale/es.json'
import enLocale from 'locale/en.json'

import './App.css'


/* Import basic support for another locale if needed ('en' is included by default) */
const esLocaleData = require('react-intl/locale-data/es');

addLocaleData(esLocaleData);

/* Define your translations */
const i18nConfig = {
    'es': {
        locale: 'es',
        messages: esLocale,
    },
    'en': {
        locale: 'en',
        messages: enLocale
    }
}


class App extends Component {
    constructor (props) {
        super(props)

        const nav = window.navigator
        const currentLocale = nav.languages && nav.languages.length > 0 ? nav.languages[0] : nav.language
        this.state = {
            lang: i18nConfig[currentLocale.substr(0, 2)] || i18nConfig.en,
            notifications: [],
            languages: i18nConfig
        }

        this.setLang = this.setLang.bind(this)
        this.setNotification = this.setNotification.bind(this)
    }

    componentDidMount () {
        this.tokenLang = Helper.PubSub.subscribe(Helper.EVENTS.SET_APP_LANG, this.setLang)
        this.tokenNotification = Helper.PubSub.subscribe(Helper.EVENTS.SET_NOTIFICATION, this.setNotification)
    }

    componentWillUnmount () {
        Helper.PubSub.unsubscribe(this.tokenLang)
        Helper.PubSub.unsubscribe(this.tokenNotification)
    }

    setLang (event, data) {
        const newLang = this.state.languages[data]
        if (newLang) {
            this.setState({ lang : newLang })
        }
    }

    setNotification (event, data) {
        this.Notification.addNotification(data)
    }

    render () {
        return (
            <IntlProvider locale={this.state.lang.locale} messages={this.state.lang.messages} key={this.state.lang.locale}>
                <div className='wrapper'>
                    <Router appInfo={this.state} />
                    <Notification ref={ref => this.Notification = ref} />
                </div>
            </IntlProvider>
        )
    }
}

export default App
