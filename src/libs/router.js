/* eslint-disable */
import React, { Component, PropTypes } from 'react'
import { Router, Route, hashHistory, IndexRoute } from 'react-router'
import Theme from 'themes/base'
import loadHome from 'bundle-loader?lazy!views/home'
import loadDetail from 'bundle-loader?lazy!views/detail'

import Render from 'libs/render'


const loadPublic = [
    { comp: 'Home', source: loadHome, path: 'home' }, // This will be set up as a default
    { comp: 'Detail', source: loadDetail, path: 'detail(/:year/:round)' }
]

const buildComp = list =>
    list.map((route) => {
        const _comp = props => (
            <Render load={route.source}>
                { Comp => <Comp {...props} /> }
            </Render>
        )
        return {
            component: _comp,
            path: route.path
        }
    })

const publicComponents = buildComp(loadPublic)

class RouterWrapper extends Component {

    render () {
        return (
            <Router history={hashHistory}>
                <Route path='/' component={Theme} appInfo={this.props.appInfo}>
                {
                    publicComponents.map((item, i) => {
                        if (i > 0) {
                            return <Route key={item.path} path={item.path} component={item.component} appInfo={this.props.appInfo} />
                        } else {
                            return <IndexRoute key='index' component={item.component} appInfo={this.props.appInfo} />
                        }
                    })
                }
                </Route>
                <Route path='*' component={Theme} appInfo={this.props.appInfo} />
            </Router>
        )
    }
}

RouterWrapper.propTypes = {
    appInfo: PropTypes.object.isRequired
}

export default RouterWrapper