import axios from 'axios'
import Config from 'config'
import jsonp from 'jsonp'

const esc = encodeURIComponent;

function makeUrl (url, params) {
    let _temp = url
    Object.keys(params).map((k) => {
        _temp = _temp.replace(`:${k}`, esc(params[k]))
    })
    return _temp
}

const api = {
    async makeCall (endpoint, params, config = {}) {
        const nameParams = endpoint.method.toLowerCase() === 'get' ? 'params' : 'data'
        Object.assign(config, endpoint, { [nameParams]: params })

        try {
            const res = await axios(config)

            return res
        } catch (e) {
            return { error: e }
        }
    },

    get (endpoint, params = {}, buildUrl, config = {}) {
        const _temp = Object.assign({}, endpoint)
        if (buildUrl) {
            _temp.url = makeUrl(_temp.url, params)
        }

        return api.makeCall(_temp, params, config)
    },

    getJSONP (endpoint, params = {}, buildUrl = false) {
        const _temp = Object.assign({}, endpoint)
        if (buildUrl) {
            _temp.url = makeUrl(_temp.url, params)
        }
        return new Promise((resolve) => {
            try {
                jsonp(_temp.url, null, (err, data) => {
                    if (err) {
                        resolve({ error: err.message })
                    } else {
                        resolve(data)
                    }
                });
            }
            catch (e) {
                resolve({ hasError: true, error: e.message })
            }

        })
    },

    axios (endpoint, data, config = {}) {
        try {

            return axios.post(endpoint.url, data, config)

        } catch (e) {
            return { error: e }
        }
    },

    ENDPOINTS: {
        'QUALIFICATION_BY_YEAR': {
            url: `${Config.apiHost}/f1/driverstandings/1.json?offset=55`,
            method: 'get'
        },
        'WINNERS_BY_SEASON': {
            url: `${Config.apiHost}/f1/:year/results/1.json`,
            method: 'get'
        },
        'QUALIFICATION_BY_RACE': {
            url: `${Config.apiHost}/f1/:year/:round/results.json`,
            method: 'get'
        }
    }
}

export default api