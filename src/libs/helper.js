import React from 'react'
import PubSub from 'pubsub-js'
import { FormattedMessage } from 'react-intl'

if (!window.localStorage) {
    window.__APP_STORAGE__ = {}
}

const events = {
    SET_APP_INFO: 'SET_APP_INFO',
    SET_APP_LANG: 'SET_APP_LANG',
    SET_PAGE_ACTIVE: 'SET_PAGE_ACTIVE',
    SET_NOTIFICATION: 'SET_NOTIFICATION',
    LOGOUT: 'LOGOUT',
    EMIT_SOCKET_EVENT: 'EMIT_SOCKET_EVENT'
}

function saveStorage (key, data) {
    if (window.localStorage) {
        window.localStorage.setItem(key, data.sort || typeof data === 'object' ? JSON.stringify(data) : data)
    } else {
        window.__APP_STORAGE__[key] = data
    }
}

function getStorage (key, isObject) {
    if (window.localStorage) {
        return isObject ? JSON.parse(window.localStorage.getItem(key) || '{}') : window.localStorage.getItem(key)
    } else {
        return window.__APP_STORAGE__[key]
    }
}

function delStorage (key) {
    if (window.localStorage) {
        return window.localStorage.removeItem(key)
    } else {
        return delete window.__APP_STORAGE__[key]
    }
}

function addNotification (message, title, level = 'error') {
    PubSub.publish(events.SET_NOTIFICATION, {
        title: title || <FormattedMessage id='app.notification' />,
        message,
        level,
        autoDismiss: 7
    });
}

function responseValidate (res) {
    if (res.MRData && !res.error && !res.hasError) {
        return true
    } else if (res.MRData && res.MRData.hasError) {
        return false
    }
    else {
        addNotification(<FormattedMessage id='app.ajaxError' />)
    }
}

export default {
    EVENTS: events,
    PubSub,
    setLocalData: saveStorage,
    getLocalData: getStorage,
    delLocalData: delStorage,
    addNotification,
    responseValidate
}