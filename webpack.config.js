var path = require('path')
var webpack = require('webpack')
var ExtractText = require('extract-text-webpack-plugin')
var HtmlPlugin = require('html-webpack-plugin')
var CopyWebpackPlugin = require('copy-webpack-plugin')

var resolve = function(dir) {
    return path.resolve(__dirname, dir)
}

var isProd = process.env.NODE_ENV === 'production'
var isDev = process.env.NODE_ENV === 'development'

var plugins = !isProd ? [
    new webpack.HotModuleReplacementPlugin()
] : [
    new webpack.optimize.UglifyJsPlugin(),
    new ExtractText('style-[contentHash:10].css')
]

plugins.push(
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        }
    }),
    new HtmlPlugin({
        template: isProd ? resolve('./production.template.html') : resolve('./index.template.html'),
    }),
    new CopyWebpackPlugin([
        isDev ? { from: './src/assets/', to: './assets/' } : { from: './src/assets/img/', to: './assets/img/' }
    ])
)

var cssIdentifier = isProd ? '[hash:base64:10]' : '[path][name]---[local]'
var cssLoader = isProd ? ExtractText.extract({
    loader: `css-loader?localIdentName=${cssIdentifier}`,
}) : ['style-loader', `css-loader?localIdentName=${cssIdentifier}`]

module.exports = {
    externals: {        
        'react/addons': true, // Do not include these externals in the bundle
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true,
        'react': 'React',
        'react-dom': 'ReactDOM',
    },
    devtool: isProd ? 'cheap-module-source-map' : 'source-map', // Allow Source Map to Debug
    entry: ['babel-polyfill', resolve('./src/index.js')],
    plugins,
    resolve: {
        modules: [resolve('./src'), resolve('./node_modules'), "node_modules"],
        extensions: ['.js'],
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loaders: ['babel-loader'],
            exclude: /(node_modules|\.spec\.js)/,
        },
        {
            test: /\.js$/,
            enforce: 'pre',
            exclude: /(node_modules|\.spec\.js)/,
            use: [
                {
                    loader: 'eslint-loader',
                    options: {
                        failOnWarning: false,
                        failOnError: false,
                        emitWarning: true,
                    },
                },
            ],
        },
        {
            test: /\.(png|jpg|gif|svg)$/,
            loaders: ['url-loader?limit=15000&name=images/[hash:8].[ext]'],
            exclude: '/node_modules/',
        },
        {
            test: /\.(css)$/,
            loaders: cssLoader,
            exclude: '/node_modules',
        },
        {
            test: /\.(eot|otf|ttf|woff|woff2)$/,
            loader: 'file-loader?name=fonts/[name].[ext]'
        }],
    },
    output: {
        path: resolve('./dist'),
        publicPath: isDev ? '/' : '/app/',
        filename: isDev ? 'bundle.js' : 'bundle-[hash:12].min.js',
    },
}
